# GCP VPC Peering terraform module
This module creates VPC peering connections between two networks, which allows internal IP connectivity across two VPC 
networks regardless of their projects or organisation belong to.

The peered networks can't have overlapping IP ranges as this would cause routing issues. The module itself doesn't check 
if there is any subnet IP range overlap. If there is an overlap, peering will not be established. 

For more information about GCP VPC Peering, please refer to:
* [VPC Network Peering overview](https://cloud.google.com/vpc/docs/vpc-peering)
* [Using VPC Network Peering](https://cloud.google.com/vpc/docs/using-vpc-peering)

Note: there is a known [issue](https://github.com/terraform-providers/terraform-provider-google/issues/3034) in the terraform module.
A simple workaround is using:
`terraform apply -parallelism=1`

Note (IAM/roles): the account with *roles/editor* or *roles/compute.networkAdmin* role can configure peering for both projects.  
 
# Usage Examples
This [examples](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-vpc-peering/-/tree/master/examples) directory contains examples of use.