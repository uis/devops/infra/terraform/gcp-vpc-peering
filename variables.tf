# variables.tf defines inputs for the module

variable "project" {
  description = "Project name"
}

variable "peered_project" {
  description = "Peered project name"
}

variable "network_name" {
  description = "Name of network"
}

variable "peered_network_name" {
  description = "Name of peered network"
}

variable "network_link" {
  description = "Self link of network"
}

variable "peered_network_link" {
  description = "Self link of peered network"
}

variable "network_route_mode" {
  description = "Route exchange mode of network"
  default     = "both" # valid values: both, import, export
}

variable "peered_network_route_mode" {
  description = "Route exchange mode of peered network"
  default     = "both" # valid values: both, import, export
}

variable "outbound_peering_name" {
  description = "Name of the outbound peering (local network to peered network)"
}

variable "inbound_peering_name" {
  description = "Name of the inbound peering (peered network to local network)"
}
