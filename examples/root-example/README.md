# Simple example
This is a simple example of creating VPC peering connections between two networks.  

```hcl-terraform
$ terraform init
$ terraform apply -var 'network_name=foo' \
                  -var 'network_link=https://www.googleapis.com/compute/v1/projects/<PROJECT_ID>/global/networks/foo' \
                  -var 'peered_network_name=bar' \
                  -var 'peer_network_link=https://www.googleapis.com/compute/v1/projects/<PROJECT_ID>/global/networks/bar' \
                  -var 'network_route_mode=both' \
                  -var 'peered_network_route_mode=both'
``` 
