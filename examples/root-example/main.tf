module "peeringAB" {
  source = "git::https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-vpc-peering.git"

  project        = "foo"
  peered_project = "bar"

  network_name = "foo"
  network_link = data.google_compute_network.network.self_link

  peered_network_name = "bar"
  peered_network_link = data.google_compute_network.peered_network.self_link

  network_route_mode        = "import"
  peered_network_route_mode = "export"
}

# Local network
data "google_compute_network" "network" {
  project = "foo"
  name    = "default"
}

# Peered network
data "google_compute_network" "peered_network" {
  project = "bar"
  name    = "default"
}