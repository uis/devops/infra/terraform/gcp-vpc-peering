# Both the google and google-beta provider are configured identically to use
# default application credentials.
provider "google" {
  version = "~> 3.13"
}

provider "google-beta" {
  version = "~> 3.13"
}
